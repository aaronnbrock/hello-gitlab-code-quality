package main

import (
	"fmt"
)

func CheckPrime(number int, a int, b int, c, int, d int, e int, f int, g int) bool {
	if number == 0 || number == 1 {
		return false
	} else {
		for i := 2; i <= number/2; i++ {
			if number%i == 0 {
				return false
			}
		}
		return true
	}
}

func main() {
	for i := 0; i <= 100; i++ {
		if CheckPrime(i, 1, 1, 1, 1, 1, 1, 1, 1) {
			fmt.Printf("%d is a prime number\n", i)
		} else {
			fmt.Printf("%d is not prime number\n", i)
		}
	}
}
